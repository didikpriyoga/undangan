function startTheJourney() { $('.top-cover').eq(0).addClass('hide'); setTimeout(function () { $('body').eq(0).css('overflow', 'visible'); $('.top-cover').eq(0).css('display', 'none'); }, 900); setTimeout(playMusic, 900); }
var $alert = $('#alert'); var $alertClose = $('#alert .alert-close'); var $alertText = $('#alert .alert-text'); function hideAlert() {
    if ($alert.hasClass('show')) { $alert.removeClass('show'); }
    if ($alert.hasClass('success')) { $alert.removeClass('success'); }
    if ($alert.hasClass('error')) { $alert.removeClass('error'); }
    $alert.addClass('hide');
}
function showAlert(message, status) {
    if ($alert.hasClass('hide')) { $alert.removeClass('hide'); }
    $alert.addClass('show'); $alert.addClass(status); $alertText.text(message); setTimeout(hideAlert, 3000);
}
var $modal = $('#modal'); var $modalContents = $('.modal-content'); function openModal() {
    $modal.html(''); if ($modal.css('display') == 'none') { $modal.css('display', 'flex'); }
    $modalContents.each(function (i, modal) { $(modal).hide(); }); $('html').css('overflow', 'hidden');
}
function closeModal() {
    if ($modal.css('display') == 'flex') { $modal.css('display', 'none'); }
    $('html').css('overflow', 'scroll'); $modal.html('');
}
$(document).on('click', '.close-modal', function (e) { e.preventDefault(); closeModal(); }); function copyToClipboard(text) { var dummy = document.createElement("textarea"); document.body.appendChild(dummy); dummy.value = text; dummy.select(); document.execCommand("copy"); document.body.removeChild(dummy); showAlert('Berhasil di salin ke papan klip', 'success'); }
function urlify(text) {
    var lineBreak = ''; var urlRegex = /(https?:\/\/[^\s]+)/g; return text.replace(urlRegex, function (url) {
        var finalURL = url; if (url.indexOf('<br>') > -1) { finalURL = url.replace(/<br>/g, ''); lineBreak = '<br>'; }
        return '<a href="' + finalURL + '" target="_blank">' + finalURL + '</a>' + lineBreak;
    });
}
$(document).on('click', '.copy-account', function (e) { e.preventDefault(); var book = $(this).closest('.book'); var number = $(book).find('.account-number'); copyToClipboard(number.html()); }); var numberFormat = new Intl.NumberFormat('ID', {}); $('img').on('dragstart', function (e) { e.preventDefault(); }); $(document).on('keyup focus', 'textarea', function (e) { e.preventDefault(); this.style.height = '1px'; this.style.height = (this.scrollHeight) + 'px'; }).on('focusout', 'textarea', function (e) { e.preventDefault(); this.style.height = 24 + 'px'; }); 
function ajaxMultiPart(data, beforeSend, callback) { $.ajax({ type: 'post', dataType: 'json', contentType: false, processData: false, data: data, beforeSend: beforeSend, success: function (result) { if (result.error === false) { callback(result); } else { showAlert(result.message, 'error'); $('.gift-next').prop('disabled', false); $('.gift-submit').prop('disabled', false); $('.gift-submit').html('Konfirmasi'); } }, }); }
$(document).on('click', '[data-modal]', function (e) {
    e.preventDefault(); var element = this; var modal = $(element).data('modal'); var data = { 'status': 'modal', 'modal': modal }
    if (modal == 'delete_comment') { var comment = $(element).data('comment'); data['comment'] = comment; }
}); $(document).on('click', '[data-delete]', function (e) {
    e.preventDefault(); var element = this; var status = $(element).data('delete'); var data = { 'status': status }; if (status == 'delete_comment') { var comment = $(element).data('comment'); data['comment'] = comment; }
}); function sliderOptions() { return { centerMode: true, slidesToShow: 1, variableWidth: true, autoplay: true, autoplaySpeed: 3000, infinite: true, speed: 500, fade: true, cssEase: 'linear', dots: false, arrows: false, pauseOnFocus: false, pauseOnHover: false, draggable: false, touchMove: false }; }; var isCoverPlayed = false; var coverSetup = (function setup(windowWidth) {
    if (typeof windowWidth == 'undefined') { windowWidth = $(window).width(); }
    if (window.DESKTOP_COVERS != '' || window.MOBILE_COVERS != '') { $('.cover-inner').addClass('covers'); } else { $('.cover-inner').removeClass('covers'); }
    if (windowWidth > '1020' && windowWidth < '1030') { isCoverPlayed = false; }
    // if (!isCoverPlayed) { $('.cover-show').each(function (i, slider) { if ($(slider).hasClass('slick-initialized')) { $(slider).not('.slick-initialized').slick({
    //     centerMode: true,
    //     infinite: true,
    //     prevArrow: false,
    //     nextArrow: false,
    //     speed: 200,
    //     slidesToShow: 1
    //   }); } }); $.each(window.COVERS, function (i, cover) { $(cover).html(''); }); $.each(window.OPENING_COVERS, function (i, cover) { $(cover).html(''); }); }
    // var smallScreen = window.matchMedia("(max-width: 1024px)"); if (!smallScreen.matches) {
    //     if (!isCoverPlayed) {
    //         if (window.DESKTOP_COVERS != '') { $('.cover-inner').removeClass('mobile').addClass('desktop'); $.each(window.COVERS, function (i, cover) { $(cover).append(window.DESKTOP_COVERS); $(cover).slick(sliderOptions()); }); isCoverPlayed = true; }
    //         if (window.DESKTOP_OPENING_COVERS != '') { $.each(window.OPENING_COVERS, function (i, cover) { $(cover).append(window.DESKTOP_OPENING_COVERS); $(cover).slick(sliderOptions()); }); }
    //     }
    // } else {
    //     if (!isCoverPlayed) {
    //         if (window.MOBILE_COVERS != '') { $('.cover-inner').removeClass('desktop').addClass('mobile'); $.each(window.COVERS, function (i, cover) { $(cover).append(window.MOBILE_COVERS); $(cover).slick(sliderOptions()); }); isCoverPlayed = true; }
    //         if (window.MOBILE_OPENING_COVERS != '') { $.each(window.OPENING_COVERS, function (i, cover) { $(cover).append(window.MOBILE_OPENING_COVERS); $(cover).slick(sliderOptions()); }); }
    //     }
    // }
    return setup;
}()); var countdown = (function count() {
    var schedule = window.SCHEDULE_EVENT; var event = new Date(schedule * 1000).getTime(); var start = setInterval(rundown, 1000); function rundown() { var now = new Date().getTime(); var distance = event - now; var days = Math.floor(distance / (1000 * 60 * 60 * 24)); var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)); var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60)); var seconds = Math.floor((distance % (1000 * 60)) / 1000); if (distance < 0) { clearInterval(start); $('.count-day').text('0'); $('.count-hour').text('0'); $('.count-minute').text('0'); $('.count-second').text('0'); } else { $('.count-day').text(days); $('.count-hour').text(hours); $('.count-minute').text(minutes); $('.count-second').text(seconds); } }
    return count;
}()); function attendanceToggle(input) {
    var attendanceCome = $('.attendance-value.come'); var attendanceNotCome = $('.attendance-value.not-come'); var come, notCome; come = typeof window.RSVP_COME != 'undefined' ? window.RSVP_COME : 'Datang'; notCome = typeof window.RSVP_NOT_COME != 'undefined' ? window.RSVP_NOT_COME : 'Ga Datang'; $(attendanceCome).html(come); $(attendanceNotCome).html(notCome); if ($(input).is(':checked')) {
        if ($(input).next('.attendance-value.come').length > 0) { $(attendanceCome).html('<i class="fas fa-smile"></i> ' + come); $('#rsvp-guest-amount').slideDown(); }
        if ($(input).next('.attendance-value.not-come').length > 0) { $(attendanceNotCome).html('<i class="fas fa-sad-tear"></i> ' + notCome); $('#rsvp-guest-amount').slideUp(); }
    }
}
$(document).on('change', '[name="attendance"]', function (e) { e.preventDefault(); attendanceToggle(this); })
$(document).on('click', '.change-confirmation', function (e) { e.preventDefault(); $('.rsvp-inner').find('.rsvp-form').fadeIn(); $('.rsvp-inner').find('.rsvp-confirm').hide(); }); $(document).on('click', '[data-quantity="plus"]', function (e) { e.preventDefault(); var fieldName = $(this).attr('data-field'); var $input = $('input[name="' + fieldName + '"]'); var currentVal = parseInt($input.val()); var bool = $input.prop('readonly'); if (!bool) { if (!isNaN(currentVal)) { if (currentVal < $input.prop('max')) { $input.val(currentVal + 1); } } else { $input.val(1); } } }); $(document).on('click', '[data-quantity="minus"]', function (e) { e.preventDefault(); var fieldName = $(this).attr('data-field'); var $input = $('input[name="' + fieldName + '"]'); var currentVal = parseInt($input.val()); var bool = $input.prop('readonly'); if (!bool) { if (!isNaN(currentVal)) { $input.val(currentVal - 1); if (currentVal <= 0) { $input.val(0); } } else { $input.val(0); } } }); $(document).on('change', '[data-quantity="control"]', function (e) { e.preventDefault(); var max = $(this).prop('max'); var value = $(this).val(); if (value > max) { $(this).val(max); } }); $(document).on('change', '[name="nominal"]', function (e) {
    e.preventDefault(); var val = $(this).val(); var input = $('.insert-nominal'); $(input).slideUp(); if (parseInt(val) <= 0) { if ($(this).is(':checked') == true) { $(input).slideDown(); $(input).find('[name="inserted_nominal"]').val('').focus(); } }
    $(input).find('[name="inserted_nominal"]').val(val);
}); $(document).on('click', '.gift-next', function (e) {
    e.preventDefault(); var form = $('#gift-form'); if ($(form).find('[name="name"]').val() == '') { $(form).find('[name="name"]').focus(); return; }
    if ($(form).find('[name="account_name"]').val() == '') { $(form).find('[name="account_name"]').focus(); return; }
    if ($(form).find('[name="message"]').val() == '') { $(form).find('[name="message"]').focus(); return; }
    if ($(form).find('[name="inserted_nominal"]').val() <= 0) { $('.insert-nominal').slideDown(); $(form).find('[name="inserted_nominal"]').focus(); return; }
    $('.gift-details').hide(); $('.gift-picture').fadeIn();
}); $(document).on('click', '.gift-back', function (e) { e.preventDefault(); $('.gift-picture').hide(); $('.gift-details').fadeIn(); }); $(document).on('submit', '#gift-form', function (e) { var data = new FormData(this); ajaxMultiPart(data, function () { $('.gift-next').prop('disabled', true); $('.gift-submit').prop('disabled', true); $('.gift-submit').html('<i class="fas fa-spinner fa-spin"></i>'); }, function (result) { $(this).trigger('reset'); showAlert(result.message, 'success'); setTimeout(function () { window.location.reload(true); }, 1000); }); return false; }); var allComments = (function comment() {
    var data = { 'status': 'all_comments', }
}()); $(document).on('click', '.more-comment', function (e) {
    e.preventDefault(); var lastComment = $(this).data('last-comment'); var data = { 'status': 'more_comments', 'last_comment': lastComment, }
    
}); var backgroundMusic = document.createElement("audio"); backgroundMusic.autoplay = true; backgroundMusic.loop = true; backgroundMusic.load(); backgroundMusic.src = window.BACKGROUND_MUSIC; var isMusicAttemptingToPlay = false
var isMusicPlayed = false
$(document).on('scroll click', function () { if (!isMusicAttemptingToPlay && !isMusicPlayed) { isMusicAttemptingToPlay = true; setTimeout(playMusic, 1000); } }); var pauseMusic = function () { isMusicAttemptingToPlay = false; var promise = backgroundMusic.pause(); isMusicPlayed = false; pauseBoxAnimation(); }
var playMusic = (function music() {
    isMusicAttemptingToPlay = false
    var promise = backgroundMusic.play(); if (promise !== undefined) { promise.then(_ => { isMusicPlayed = true; playBoxAnimation(); }).catch(error => { isMusicPlayed = false; pauseBoxAnimation(); }); }
    return music;
}()); function playBoxAnimation() {
    var box = $('#music-box'); if (!$(box).hasClass('playing')) { $(box).addClass('playing'); }
    if ($(box).css('animationPlayState') != 'running') { $(box).css('animationPlayState', 'running'); }
}
function pauseBoxAnimation() { var box = $('#music-box'); if ($(box).hasClass('playing')) { if ($(box).css('animationPlayState') == 'running') { $(box).css('animationPlayState', 'paused'); } } }
$(document).on('click', '#music-box', function (e) { e.preventDefault(); if (isMusicPlayed) { pauseMusic(); isMusicAttemptingToPlay = true; } else { playMusic(); } }); 